return {
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "BurntSushi/ripgrep",
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "make",
            },
            "sharkdp/fd",
            "nvim-tree/nvim-web-devicons",
            "folke/which-key.nvim",
        },
        config = function(_, opts)
            local telescope = require('telescope')
            local wk = require('which-key')

            telescope.setup({
                extensions = {
                    fzf = {
                        fuzzy = true,
                        ovveride_generic_sorter = true,
                        override_file_sorter = true,
                        case_mode = 'smart_case',
                    }
                }
            })

            -- telescope.load_extension('fzf')

            local builtin = require('telescope.builtin')

            wk.register({
                f = {
                    name = 'find',
                    f = { builtin.find_files, 'Find File' },
                    r = { builtin.oldfiles, 'Open Recent File' },
                    s = { builtin.grep_string, 'Grep String in cwd' },
                    l = { builtin.live_grep, 'Live Grep' },
                    b = { builtin.buffers, 'Buffer' },
                    t = { builtin.tags, 'Ctags' },
                },
            }, { prefix = '<leader>' })
        end
    }
}
