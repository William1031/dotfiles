return {
    {
        "William103/tree-sitter-curse",
        config = true,
    },
    {
        "mrcjkb/haskell-tools.nvim",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope.nvim"
        },
        tag = "1.11.3",
        ft = "haskell",
        config = function()
            local ht = require('haskell-tools')
            local wk = require('which-key')
            ht.start_or_attach {
                hls = {
                    on_attach = function(client, bufnr)
                        wk.register({
                            c = { vim.lsp.codelens.run, "Code Lens" },
                            v = { ht.lsp.buf_eval_all, "Eval All" },
                        }, {
                            prefix = "<leader>l",
                        })
                        wk.register({
                            h = { ht.hoogle.hoogle_signature, "Hoogle Signature" },
                        }, {
                            prefix = "<leader>f",
                        })
                        require('servers.lsp.handlers').on_attach(client, bufnr)
                    end,
                },
            }

            local bufnr = vim.api.nvim_get_current_buf()

            wk.register({
                r = {
                    name = "REPL",
                    r = { ht.repl.toggle, "Toggle current package" },
                    f = { function()
                        ht.repl.toggle(vim.api.nvim_buf_get_name(bufnr))
                    end, "Toggle current buffer" },
                    q = { ht.repl.quit, "Quit" },
                }
            }, {
                prefix = "<leader>"
            })
        end
    }
}
