local servers = {
    "clangd",
    "rust_analyzer",
    "zls",
}

return {
    {
        "neovim/nvim-lspconfig",
        event = "BufReadPre",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "folke/which-key.nvim",
            "nvim-telescope/telescope.nvim",
        },

        opts = {
            servers = servers
        },

        config = function(_, opts)
            local lspconfig = require("lspconfig")
            local handlers = require("servers.lsp.handlers")
            
            local xopts = {}
            for _, server in ipairs(opts.servers) do
                xopts = {
                    on_attach = handlers.on_attach,
                    capabilities = handlers.capabilities,
                    keymaps = handlers.lsp_keymaps,
                }
                server = vim.split(server, '@')[1]
                local ok, copts = pcall(require, "servers.lsp.servers" .. server)
                if ok then
                    xopts = vim.tbl_deep_extend('force', copts, xopts)
                end
                lspconfig[server].setup(xopts)
            end
        end,
    },

    { "j-hui/fidget.nvim", event = "VeryLazy", config = true, tag = "legacy" },

    {
        "ray-x/lsp_signature.nvim",
        event = "BufReadPre",
        opts = {
            floating_window = false,
            hint_prefix = '',
        }
    },

    {
        "jose-elias-alvarez/null-ls.nvim",
        config = true,
        requires = { "nvim-lua/plenary.nvim" },
    },
}
