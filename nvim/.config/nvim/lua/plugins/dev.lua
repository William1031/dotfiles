return {
    {
        "akinsho/toggleterm.nvim",
        opts = {
            open_mapping = [[<C-\>]],
            direction = "horizontal",
            insert_mappings = true,
            start_in_insert = true,
        },
        keys = [[<C-\>]],
    },

    -- LANGUAGES
    {
        "simrat39/rust-tools.nvim",
        ft = "rust",
        dependencies = {
            "saecki/crates.nvim",
            "nvim-lua/plenary.nvim",
            "folke/which-key.nvim",
        },
        opts = {
            server = {
                on_attach = function(client, bufnr)
                    local keymap = vim.api.nvim_buf_set_keymap
                    local opts = { noremap = true, silent = true }
                    require('servers.lsp.handlers').on_attach(client, bufnr)
                    keymap(bufnr, 'n', 'K', [[<cmd>lua require('rust-tools').hover_actions.hover_actions()<CR>]], opts)

                    local rt = require("rust-tools")
                    require("which-key").register({
                        r = {
                            name = "rust",
                            r = { rt.runnables.runnables, "Runnables" },
                            m = { rt.expand_macro.expand_macro, "Expand Macro" },
                        }
                    }, { prefix = "<leader>" })
                end
            }
        }
    },

    {
        "saecki/crates.nvim",
        config = true,
        event = "BufEnter Cargo.toml",
    }
}
