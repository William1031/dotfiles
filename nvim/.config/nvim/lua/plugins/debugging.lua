local debug_filetypes = { "rust" }

return {
    {
        "mfussenegger/nvim-dap",
        dependencies = {
            "folke/which-key.nvim",
        },
        ft = debug_filetypes,
        config = function(_, opts)
            local wk = require("which-key")
            local dap = require("dap")

            wk.register({
                d = {
                    name = "debug",
                    t = { dap.toggle_breakpoint, "Toggle Breakpoint" },
                    c = { dap.continue, "Continue" },
                    o = { dap.step_over, "Step Over" },
                    i = { dap.step_into, "Step Into" },
                    r = { dap.repl.open, "Open Repl" },
                },
            }, { prefix = "<leader>" })

        end
    },

    {
        "rcarriga/nvim-dap-ui",
        dependencies = {
            "mfussenegger/nvim-dap",
        },
        ft = debug_filetypes,
        opts = {
            expand_lines = true,
            mappings = {
                expand = { "<CR>" },
                open = "o",
                remove = "d",
                edit = "e",
                repl = "r",
                toggle = "t",
            },
        },
        config = function(_, opts)
            local dap = require("dap")
            local dapui = require("dapui")
            dapui.setup(opts)

            dap.listeners.after.event_initialized["dapui_config"] = dapui.open
            dap.listeners.before.event_terminated["dapui_config"] = dapui.close
            dap.listeners.before.event_exited["dapui_config"] = dapui.close
        end
    }
}
