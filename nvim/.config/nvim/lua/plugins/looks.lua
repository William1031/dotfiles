return {
    {
        "catppuccin/nvim",
        lazy = false,
        priority = 1000,
        config = function(_, opts)
            vim.cmd([[colorscheme catppuccin]])

            vim.cmd([[highlight Normal ctermbg=NONE guibg=NONE]])
            vim.cmd([[highlight EndOfBuffer ctermbg=NONE guibg=NONE]])
            vim.cmd([[highlight StatusLine ctermbg=NONE guibg=NONE]])
        end
    },

    { 
        "norcalli/nvim-colorizer.lua",
        opts = "*",
    },

    "stevearc/dressing.nvim",
}
