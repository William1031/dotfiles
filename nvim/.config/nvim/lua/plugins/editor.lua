function simple(str)
    return {
        str,
        config = true
    }
end

return {
    simple("numToStr/Comment.nvim"),

    {
        "NeogitOrg/neogit",
        dependencies = { "nvim-lua/plenary.nvim" },
        cmd = "Neogit",
        config = true,
    },

    "jghauser/mkdir.nvim",

    simple("kylechui/nvim-surround"),

    simple("windwp/nvim-autopairs"),
}
