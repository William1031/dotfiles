return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        event = { "BufReadPost", "BufNewFile" },

        dependencies = "mrjones2014/nvim-ts-rainbow",

        opts = {
            highlight = { enable = true },
            indent = { enable = true },
            rainbow = {
                enable = true,
                extended_mode = false,
                max_file_lines = nil,
            },
            context_commentstring = {
                enable = true,
                enable_autocmd = false
            },
            ensure_installed = {
                "bash",
                "bibtex",
                "c",
                "cpp",
                "diff",
                "fish",
                "gitignore",
                "haskell",
                "html",
                "julia",
                "lalrpop",
                "lua",
                "make",
                "markdown",
                "markdown_inline",
                "python",
                "regex",
                "rust",
                "scheme",
                "toml",
                "yaml",
                "zig",
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = 'gnn',
                    node_incremental = 'grn',
                    scope_incremental = 'grc',
                    node_decremental = 'grm',
                }
            },
        },

        config = function(_, opts)
            require("nvim-treesitter.configs").setup(opts)
            require("nvim-treesitter.parsers").get_parser_configs().curse = {
                install_info = {
                    url = "/home/wball/code/tree-sitter/tree-sitter-curse/",
                    files = { "src/parser.c" },
                }
            }
        end,
    },
}

-- local parser_configs =
-- require('nvim-treesitter.parsers').get_parser_configs()
-- 
-- require('nvim-treesitter.configs').setup({
--     ensure_installed = {'bash', 'bibtex', 'c', 'cpp', 'diff',
--     'fish', 'gitignore', 'haskell', 'java', 'julia', 'lalrpop',
--     'latex', 'lua', 'make', 'markdown', 'markdown_inline',
--     'python', 'r', 'regex', 'rust', 'scheme', 'toml', 'yaml',
--     'zig'},
-- 
--     highlight = {
--         enable = true,
--         additional_vim_regex_highlighting = false,
--     },
-- 
--     incremental_selection = {
--         enable = true,
--         keymaps = {
--             init_selection = 'gnn',
--             node_incremental = 'grn',
--             scope_incremental = 'grc',
--             node_decremental = 'grm',
--         }
--     },
-- 
--     indent = {
--         enable = true
--     },
-- 
--     rainbow = {
--         enable = true,
--         extended_mode = false,
--         max_file_lines = nil,
--     }
-- })
-- 
-- vim.opt.foldmethod = 'expr'
-- vim.cmd('set foldexpr=nvim_treesitter#foldexpr()')
-- vim.opt.foldenable = false
