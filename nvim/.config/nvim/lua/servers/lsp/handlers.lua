local M = {}

local function lsp_keymaps(bufnr)
    local wk = require('which-key')
    local ts = require('telescope.builtin')

    local opts = { noremap = true, silent = true }
    local keymap = vim.api.nvim_buf_set_keymap

    wk.register({
        l = {
            name = "lsp",
            d = { vim.lsp.buf.type_definition, "Type Definition" },
            k = { vim.lsp.buf.signature_help, "Signature Help" },
            q = { vim.diagnostic.setloclist, "Set loclist" },
            f = { vim.lsp.buf.format, "Format" },
            a = { vim.lsp.buf.code_action, "Code Action" },
            r = { vim.lsp.buf.rename, "Rename" },
            e = { vim.diagnostic.open_float, "Line Diagnostic" },
        }
    }, {
        prefix = "<leader>",
    })

    wk.register({
        s = { ts.lsp_document_symbols, "Symbol" },
        w = { ts.lsp_dynamic_workspace_symbols, "Workspace Symbol" },
        r = { ts.lsp_references, "References" },
    }, {
        prefix = "<leader>f",
    })

    wk.register({
        D = { vim.lsp.buf.declaration, "Go to declaration" },
        d = { vim.lsp.buf.definition, "Go to definition" },
        i = { vim.lsp.buf.implementation, "Go to implementation" },
        r = { vim.lsp.buf.references, "Go to references" },
    }, { prefix = "g" })

    keymap(bufnr, 'n', 'K', [[<cmd>lua vim.lsp.buf.hover()<CR>]], opts)

    keymap(bufnr, 'n', '[d', [[<cmd>lua vim.diagnostic.goto_prev()<CR>]], opts)
    keymap(bufnr, 'n', ']d', [[<cmd>lua vim.diagnostic.goto_next()<CR>]], opts)
end

M.on_attach = function(client, buffer)
    lsp_keymaps(buffer)
end

M.capabilities = vim.lsp.protocol.make_client_capabilities()

local ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if ok then
    M.capabilities = cmp_nvim_lsp.default_capabilities(M.capabilities)
end

return M
