-- turn on cursorline
vim.opt.cursorline = true

-- show incremental commands in a new split
vim.opt.inccommand = 'split'

-- make external clipboard default
vim.opt.clipboard = 'unnamedplus'

-- tab characters are interpreted as 4 spaces
vim.opt.tabstop = 4

-- indent with 4 spaces
vim.opt.shiftwidth = 4

-- make tabs actually spaces
vim.opt.expandtab = true

-- allow for conceal stuff (good for LaTeX)
vim.opt.conceallevel = 1

-- good wrapping
vim.opt.breakindent = true
vim.opt.linebreak = true

-- turn on number and relativenumber
vim.opt.number = true
vim.opt.relativenumber = true

-- only redraw when necessary (helps with lag)
vim.opt.lazyredraw = true

-- ignore case when searching unless a capital letter is used
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- keep at least five lines above and below cursor on screen
vim.opt.scrolloff = 5

-- better completion menu
vim.opt.completeopt = 'menu,menuone,noselect,noinsert'

-- make statusline disappear
vim.opt.laststatus = 0

-- remove commandline
vim.opt.cmdheight = 1

-- Let neovim use all the colors
-- I thought this was set by default, but then dressing yelled at me
vim.opt.termguicolors = true

-- set guifont to my goto font
vim.opt.guifont = 'Iosevka Nerd Font Mono:h10'

-- turn off line numbers in terminal buffer
vim.api.nvim_create_autocmd("TermOpen", {
    pattern = "*",
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})

-- we use LaTeX, not plain TeX
vim.g.tex_flavor = 'latex'

-- don't replace subscripts or superscripts when editing LaTeX
vim.g.tex_conceal='abdmg'
