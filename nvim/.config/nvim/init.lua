vim.loader.enable()

require('core.options')
require('core.keybindings')
require('core.bootstrap')
