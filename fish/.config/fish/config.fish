#!/bin/fish

# fish settings
fish_hybrid_key_bindings
set fish_cursor_default block
set fish_cursor_insert line
set fish_curser_replace_one underscore
set fish_greeting

# starship prompt
eval (starship init fish)

# my aliases
abbr --add grep rg
abbr --add ls eza
abbr --add sl eza
abbr --add ll eza -alF
abbr --add la eza -a
abbr --add l eza
abbr --add lal eza -al
abbr --add cat bat

alias tmux 'tmux -f ~/.config/tmux/tmux.conf'

# add some stuff to the path
fish_add_path $HOME/.scripts
fish_add_path $HOME/.local/share/cargo/bin
fish_add_path $HOME/.local/bin
fish_add_path $HOME/.ghcup/bin
fish_add_path $HOME/.cabal/bin
fish_add_path $HOME/code/j/j9.4/bin

# set some environment variables 
set -x INPUTRC $HOME/.config/readline/inputrc
set -x EDITOR nvim
set -x VISUAL nvim
set -x TZ America/Los_Angeles
set -x GOPATH $HOME/code/go
set -x BROWSER firefox
set -x MOZ_ENABLE_WAYLAND 1
set -x TERM foot
set -x MAKEFLAGS "-j$(nproc)"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"
set -x MANROFFOPT "-c"
set -x LESSHISTFILE -
set -x GDK_BACKEND wayland
set -x XDG_DATA_HOME $HOME/.local/share
set -x CARGO_HOME $XDG_DATA_HOME/cargo
set -x GHCUP_USE_XDG_DIRS true
set -x JULIA_DEPOT_PATH $XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH
set -x JUPYTER_CONFIG_DIR $XDG_CONFIG_HOME/jupyter
set -x RUSTUP_HOME $XDG_DATA_HOME/rustup
set -x DOT_SAGE $XDG_CONFIG_HOME/sage

# make fzf catppuccin
set -Ux FZF_DEFAULT_OPTS "\
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

# make bat catppuccin
set -Ux BAT_THEME "Catppuccin-mocha"
