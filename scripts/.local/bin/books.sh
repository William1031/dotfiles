#!/bin/sh

prefix=$HOME/books/

choice=$(find $prefix -type f | sed "s|$prefix||;s/_/ /g" | fuzzel -d)

test -n "$choice" && zathura $(echo $choice | sed "s/ /_/g;s|^|$prefix|")
