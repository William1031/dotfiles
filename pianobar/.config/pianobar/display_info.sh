#!/bin/bash

while read L; do
	k="`echo "$L" | cut -d '=' -f 1`"
	v="`echo "$L" | cut -d '=' -f 2`"
	export "$k=$v"
done < <(grep -e '^\(title\|artist\|album\|stationName\|songStationName\|pRet\|pRetStr\|wRet\|wRetStr\|songDuration\|songPlayed\|rating\|coverArt\|stationCount\|station[0-9]*\)=' "$HOME/.config/pianobar/info")

curl "$coverArt" --silent --output "$HOME/.config/pianobar/album.jpg"

notify-send "Now playing:" "$title by $artist on $album" --icon="$HOME/.config/pianobar/album.jpg"
