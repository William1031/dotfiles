#!/bin/bash

choice=$(cat "$HOME/.config/pianobar/info" | grep "station[0-9]" | cut -d "=" -f 2 | fuzzel --dmenu)

[ -z "$choice" ] || echo "s$(cat "$HOME/.config/pianobar/info" | grep "$choice" | cut -d "=" -f 1 | sed "s/station//")" > "$HOME/.config/pianobar/ctl"
