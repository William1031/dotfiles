#!/bin/bash

rm "$HOME/.config/pianobar/info"
while read L; do
    echo "$L" >> "$HOME/.config/pianobar/info"
	k="`echo "$L" | cut -d '=' -f 1`"
	v="`echo "$L" | cut -d '=' -f 2`"
	export "$k=$v"
done < <(grep -e '^\(title\|artist\|album\|stationName\|songStationName\|pRet\|pRetStr\|wRet\|wRetStr\|songDuration\|songPlayed\|rating\|coverArt\|stationCount\|station[0-9]*\)=' /dev/stdin)

case "$1" in
    songstart)
        curl "$coverArt" --silent --output "$HOME/.config/pianobar/album.jpg"
        notify-send "Now playing:" "$title by $artist on $album" --icon="$HOME/.config/pianobar/album.jpg"
        ;;
    *)
        ;;
esac
