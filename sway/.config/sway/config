# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term foot
set $browser firefox
set $emacs "emacsclient --create-frame --alternate-editor='emacs'"
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu fuzzel | xargs swaymsg exec --
# set $menu dmenu_path | dmenu | xargs swaymsg exec --

### Appearance
# window corner radius in px
corner_radius 0

# Window background blur
blur off
blur_xray off
blur_passes 2
blur_radius 5

shadows enable
shadows_on_csd off
shadow_blur_radius 20
shadow_color #0000007F

# inactive window fade amount. 0.0 = no dimming, 1.0 = fully dimmed
default_dim_inactive 0.0
dim_inactive_colors.unfocused #000000FF
dim_inactive_colors.urgent #900000FF

# Move minimized windows into Scratchpad (enable|disable)
scratchpad_minimize disable

smart_gaps on
gaps inner 10
default_border pixel 2

client.focused #f5c2e7 #1e1e2e #cdd6f4 #a6e3a1 #f5c2e7
client.unfocused #1e1e2e #1e1e2e #cdd6f4 #a6e3a1 #586e75
client.focused_inactive #1e1e2e #1e1e2e #cdd6f4 #a6e3a1 #586e75

font pango:Iosevka Nerd Font 10

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output * bg ~/.config/wallpaper.png fill
output * scale 1.25
#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
exec swayidle -w \
         timeout 300 'swaylock -f -c 000000' \
         timeout 600 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
         before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
input "type:touchpad" {
    dwt enabled
    tap enabled
    natural_scroll enabled
    accel_profile adaptive
}

input "type:keyboard" {
    xkb_layout us,us
    xkb_variant dvorak,
    xkb_options "altwin:swap_lalt_lwin,grp:alt_space_toggle"
    repeat_delay 300
    repeat_rate 40
}

seat * hide_cursor 3000
seat * hide_cursor when-typing enable

#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

bindgesture swipe:right workspace prev
bindgesture swipe:left workspace next

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    bindsym $mod+bracketright exec $browser
    bindsym $mod+e exec $emacs
    bindsym $mod+d exec "makoctl dismiss"
    bindsym $mod+Shift+d exec "makoctl dismiss --all"
    bindsym $mod+z exec "books.sh"
    bindsym $mod+Control+k exec 'brightnessctl set +5%'
    bindsym $mod+Control+j exec 'brightnessctl set 5%-'
    bindsym $mod+Shift+o exec 'swaylock -f -c 000000'
    bindsym XF86AudioRaiseVolume exec 'wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+'
    bindsym XF86AudioLowerVolume exec 'wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-'
    bindsym XF86AudioMute exec 'wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle'
    bindsym XF86AudioMicMute exec 'wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle'

    # Kill focused window
    bindsym $mod+Shift+c kill

    # Start your launcher
    bindsym $mod+semicolon exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+r reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+q exit
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+t layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+slash move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+slash scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $mod+$left resize shrink width 10px
    bindsym $mod+$down resize grow height 10px
    bindsym $mod+$up resize shrink height 10px
    bindsym $mod+$right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#
# pianobar mode:
#
mode "pianobar" {
    bindsym $mod+s exec "~/.config/pianobar/change_station.sh" ; mode "default"
    bindsym $mod+p exec "echo 'p' > ~/.config/pianobar/ctl" ; mode "default"
    bindsym $mod+c exec "~/.config/pianobar/display_info.sh" ; mode "default"
    bindsym $mod+n exec "echo 'n' > ~/.config/pianobar/ctl" ; mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+p mode "pianobar"

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    swaybar_command waybar
}

# autostart
exec blueman-applet
exec mako

include /etc/sway/config.d/*
